#!/sbin/python3

import os
from lib.logs import Logger

logs = Logger()


class Find:
    def __init__(self, path='', ext='.xml'):
        self.path = path
        self.ext = ext

    def find(self, path, ext):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        logs.log().debug('DirPath: {}'.format(dir_path))

        logs.log().debug('StoredPath: {}'.format(path))
        folder = os.path.join(dir_path, path)
        logs.log().debug('Result Path: {}'.format(folder))

        for root, dirs, files in os.walk(folder):
            for file in files:
                if file.endswith(ext):
                    path_name = '{}/{}'.format(root, str(file))
                    yield path_name, file

    def to_dict(self, path, ext) -> dict:
        """
        Dict with all funded files name and index
        @return: dict
        """
        dict_on_load = {}

        for item in self.find(path, ext):
            # print(item[0], item[1])
            fn = str(item[1])[:-4]
            index = ''
            for ite in fn:
                if ite.isdigit():
                    index += ite
                dict_on_load[item[0]] = index
            logs.log().debug('File name for {} with path: {}|Index: {}'.format(ext, item[0], index))

            logs.log().debug('Keys: {}|Values: {}'.format(list(dict_on_load.keys()), list(dict_on_load.values())))
        logs.log().info('Find count files: {}'.format(len(dict_on_load.keys())))

        return dict_on_load

    def to_list(self, path, ext) -> list:
        """
        List with all find file name with os path for it
        @return: list
        """
        list_on_load = []
        for item in self.find(path, ext.upper()):
            logs.log().debug('File for {} with path and name: {}'.format(ext, item[0]))
            list_on_load.append(item[0])

        return list_on_load


def main():
    print('Hello!')


if __name__ == '__main__':
    main()
