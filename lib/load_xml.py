#! /env/python3

import xml.etree.ElementTree as ET
from lib.db import DB
from lib.logs import Logger
from lib.find_files import Find
from datetime import datetime


logs = Logger()


class XML:
    def __init__(self, path='../upload/xml', name='xml'):
        self.path = path
        self.name = name
        logs.log().info('Init load_xml module... path: {}| file: {}'.format(path, name))

    def open_xml_sax(self):
        list_on_load = Find().to_list(self.path, self.name)
        logs.log().debug('Count find xml files: {}'.format(len(list_on_load)))

        table_name = ''
        fields = ''
        data = ()
        count = 0
        db = DB()
        conn = db.connect()
        for file in list_on_load:
            start = datetime.now()

            for event, elem in ET.iterparse(file, events=('start', 'end')):
                # logs.log().debug('Event: {}|Element: {}'.format(event, elem))
                if event == 'start':
                    attrib = elem.attrib

                    if attrib:
                        # logs.log().debug('Element tag: {}|Attrib: {}'.format(elem.tag, attrib))
                        fields = ','.join(attrib.keys())

                        count = len(attrib.keys())
                        data = tuple(attrib.values())
                    else:
                        table_name = elem.tag

                    if fields:
                        insert = 'INSERT INTO {}'.format(table_name)
                        insert = '{} ({}) VALUES ({})'.format(insert, fields, ','.join(','.join('?') * count))

                        db.insert(conn, insert, tuple_d=(data,))
                #
                # elif event == 'end':
                #     # process the tag
                #     pass
            logs.log().info('Time for load XML: {}|Time: {}'.format(file, datetime.now() - start))
        db.close(conn)


def main():
    print('Start LOAD_XML module.')


if __name__ == '__main__':
    main()
