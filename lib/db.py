#!/usr/bin/python3

import os
import sqlite3
from lib.logs import Logger

logs = Logger()


class DB:
    def __init__(self, filename='main.db', conn=''):
        self.filename = filename
        self.conn = conn

    def close(self, conn):
        logs.log().debug('Close connection: {}'.format(conn))
        conn.close()

    def connect(self, filename='main.db', **kwargs):
        path = os.curdir
        dbname = '{}/db/{}'.format(path, filename)
        conn = sqlite3.connect(dbname)
        logs.log().debug('Create new connection: {}'.format(conn))

        return conn

    def create(self, conn, table, **kwargs):
        if conn:
            logs.log().debug('Connection is ready: {}'.format(conn))
        else:
            conn = self.connect()
        cur = conn.cursor()
        logs.log().debug('SQL: {}'.format(table))

        cur.execute(table)
        conn.commit()

    def select(self, conn, select, **kwargs):
        if conn:
            logs.log().debug('Connection is ready: {}'.format(conn))
        else:
            conn = self.connect()
        cur = conn.cursor()
        cur.execute(select)

    def insert(self, conn, insert, **kwargs):
        if conn:
            logs.log().debug('Connection is ready: {}'.format(conn))
        else:
            logs.log().debug('Create new connection from insert function')
            conn = self.connect()
        cur = conn.cursor()
        logs.log().debug('INSERT: {}'.format(insert))

        if kwargs:
            tuple_d = kwargs['tuple_d']
            logs.log().debug('TUPLES: {}'.format(tuple_d))
            try:
                cur.executemany(insert, tuple_d)
            except ValueError as ve:
                logs.log().error(ve)
            except sqlite3.OperationalError as oe:
                logs.log().error(oe)
            finally:
                conn.commit()
        else:
            try:
                cur.execute(conn, insert)
            except ValueError as ve:
                logs.log().error(ve)
            finally:
                conn.commit()

    def delete(self, conn, delete, **kwargs):
        if conn:
            logs.log().debug('Connection is ready: {}'.format(conn))
        else:
            conn = self.connect()
        cur = conn.cursor()
        logs.log().debug('SQL: {}'.format(delete))

        cur.execute(delete)
        conn.commit()

    def update(self, conn, update, **kwargs):
        if conn:
            logs.log().debug('Connection is ready: {}'.format(conn))
        else:
            conn = self.connect()
        cur = conn.cursor()
        cur.execute(conn, update)
        conn.commit()


def main():
    logs.log().debug('Hello World')


if __name__ == '__main__':
    main()
