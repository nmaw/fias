#! /env/python3

import operator
import xml.etree.ElementTree as ET
from lib.db import DB
from lib.logs import Logger
from datetime import datetime
from lib.find_files import Find


logs = Logger()


class XSD:
    def __init__(self, path='../upload/xsd', name='xsd'):
        self.path = path
        self.name = name
        logs.log().info('Init load_xsd module... path: {}| file: {}'.format(path, name))

    def _get_sqlite_type(self, string_from_xsd):
        type_dict = {
                    'string':  'TEXT',
                    'number':  'NUM',
                    'integer': 'INT',
                    'int':     'INT',
                    'byte':    'INT',
                    'real':    'REAL',
                    'blob':    'NONE'}
        value = type_dict[string_from_xsd]
        return value

    def open_xsd(self):
        start = datetime.now()
        dict_on_load = Find().to_dict(self.path, self.name)

        logs.log().debug('Open files from received dict: {}'.format(len(dict_on_load)))
        sort_dict_on_load = sorted(dict_on_load.items(), key=operator.itemgetter(1))
        for item in sort_dict_on_load:
            try:
                tree = ET.parse(item[0])
                root_xsd = tree.getroot()
                logs.log().debug(
                    'Tag: {}| Attrib: {}| Child: {}'.format(root_xsd.tag, root_xsd.attrib, root_xsd.getchildren()))
            except FileNotFoundError as fnfe:
                logs.log().debug(fnfe)
                root_xsd = 'N/A'
            self.parse_xsd(root_xsd)
        logs.log().info('Time for load all XSD: {}'.format(datetime.now() - start))

    def parse_xsd(self, main_element):
        # Find correct NameSpace
        ns = '{http://www.w3.org/2001/XMLSchema}'

        logs.log().debug('Create structure table: dict')
        table = {}
        table_id = main_element.getchildren()
        for i in table_id:
            table_name = i.attrib['name']
        for item_ct in main_element.findall('.//{}complexType'.format(ns)):
            logs.log().debug('Child for complexType: {}'.format(item_ct.getchildren()))
            for item in item_ct.getchildren():
                logs.log().debug('Find children: {}'.format(item.tag))

                field_type = 'REAL'
                if 'sequence' in item.tag:
                    # table_name = item.find('.//{}element'.format(ns)).attrib['name']
                    table_name = i.attrib['name']
                    logs.log().debug('Table name: {}'.format(table_name))
                    table = {'table': table_name}
                elif 'attribute' in item.tag:
                    logs.log().debug('|--> {}'.format(item.attrib['name']))
                    if item.find('.//{}simpleType'.format(ns)):
                        for itemST in item.find('.//{}simpleType'.format(ns)):
                            field_type = self._get_sqlite_type(itemST.attrib['base'][3:])
                    else:
                        field_type = item.attrib['type'][3:]
                    logs.log().debug('{}: {}'.format(item.attrib['name'], field_type))
                    table = {**table, item.attrib['name']: field_type}

                else:
                    logs.log().debug('Unknow children: {}'.format(item))
        # structure: {table: 'table_name', 'field_name': 'field_type', ...}}
        self.create_table(table)

    def create_table(self, table_structure):
        logs.log().debug('Received table structure: {}'.format(table_structure))

        fields = ''
        name_t = ''

        for name_f, type_f in table_structure.items():

            if name_f == 'table':
                name_t = type_f
            else:
                fields = '{}, {} {}'.format(fields, name_f, type_f)

        drop_sql = '''DROP TABLE IF EXISTS {};'''.format(name_t)
        create_sql = '''CREATE TABLE IF NOT EXISTS {} ({})'''.format(name_t, fields[2:])

        db = DB()
        logs.log().debug('ID DB: {}'.format(db))
        conn = db.connect()
        db.delete(conn, drop_sql)
        db.create(conn, create_sql)
        db.close(conn)

    def put_version(self):
        # TODO put new version xsd to version table in sqlite
        pass


def main():
    print('Start module load xsd')


if __name__ == '__main__':
    main()

