#!/usr/bin/python3

import os
import logging


class Logger:
    def __init__(self,
                 level='DEBUG',
                 path=os.curdir,
                 file='main.log'):
        self.log_level = level
        self.log_path = '{}/{}'.format(path, 'logs')
        self.log_file = '{}/{}'.format(self.log_path, file)

    def log(self):
        logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                            level=self.log_level, filename=self.log_file)

        return logging


def main():
    print('load logging module')


if __name__ == '__main__':
    main()
