#!/env/python3

import dbf
from pathlib import Path
# from sqlite_utils import Database
from lib.db import DB
from lib.find_files import Find
from lib.logs import Logger


logs = Logger()


class DBF:
    def __init__(self, path='../upload/dbf', name='dbf'):
        self.path = path
        self.name = name
        logs.log().info('Init load_dbf module... path: {}| file: {}'.format(path, name))

    def open(self):
        list_on_load = Find().to_list(path=self.path, ext=self.name)
        print(list_on_load)
        for path in list_on_load:
            table_name = Path(path).stem
            print(table_name)
            table = dbf.Table(str(path))
            table.open()
            www = table.codepage
            columns = table.field_names
            print(columns, www)
            for row in table:
                print(row)

    def find(self, sqlite_db, table, verbose):

        if table and len(self.path) > 1:
            raise print('Only works with a single DBF file')

        # db = Database(sqlite_db)
        db = DB()
        for path in dbf_paths:
            table_name = table if table else Path(path).stem
            if verbose:
                print('Loading {} into table "{}"'.format(path, table_name))
            table = dbf.Table(str(path))
        #     table.open()
        #     columns = table.field_names
        #     db[table_name].insert_all(dict(zip(columns, list(row))) for row in table)
        #     table.close()
        # db.vacuum()


def main():
    print('load_dbf module')


if __name__ == '__main__':
    main()
