#!/usr/bin/python3

from lib.db import DB
from lib.logs import Logger
from lib.load_xsd import XSD
from lib.load_xml import XML
from lib.load_dbf import DBF


def main():

    xsd = XSD()
    xsd.open_xsd()

    xml_f = XML()
    xml_f.open_xml_sax()

    # dbf_f = DBF()
    # dbf_f.open()
# TODO On base xsd create sqlite DB
# TODO Load dbf/xml to sqlite DB


if __name__ == '__main__':
    main()
